/*
 * Copyright (C) 2019 Trinity College Dublin - CONNECT Centre.
 * This file is part of XVL <https://bitbucket.org/joaofelipesantos/xvl/>.
 *
 * XVL is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Commercial Licenses are also available from Trinity College Dublin.
 * 
 * XVL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with XVL. If not, see <http://www.gnu.org/licenses/>.
 */
#include "xvl_main.h"

int main()
{
  // Receiver
  // Centre frequency in Hz
  double d_rx_centre_freq = 2e9;
  // Sampling rate in mega samples per second
  double d_rx_samp_rate = 20e6;
  // FFT size
  unsigned int u_rx_fft_size = 2000;

  // Transmitter
  // Centre frequency in Hz
  double d_tx_centre_freq = 2e9;
  // Sampling rate in mega samples per second
  double d_tx_samp_rate = 20e6;
  // FFT size
  unsigned int u_tx_fft_size = 2000;

  // Control port
  unsigned int u_port = 5000;

  // Instantiate XVL
  xvl main(u_port);

  // Configure the RX radio
  main.set_rx_config(d_rx_centre_freq,
                     d_rx_samp_rate,
                     u_rx_fft_size);

  // Configure the TX radio
  main.set_tx_config(d_tx_centre_freq,
                     d_tx_samp_rate,
                     u_tx_fft_size);

  // Run server
  main.run();

  // Run server
  return 0;
}
