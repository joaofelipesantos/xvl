#! /usr/bin/python3

# Copyright (C) 2019 Trinity College Dublin - CONNECT Centre.
# This file is part of XVL <https://bitbucket.org/joaofelipesantos/xvl/>.
#
# XVL is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# Commercial Licenses are also available from Trinity College Dublin.
# 
# XVL is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with XVL. If not, see <http://www.gnu.org/licenses/>.

import socket
import struct

UDP_IP = "127.0.0.1"
UDP_PORT = 5000

sock = socket.socket(socket.AF_INET, # Internet
                     socket.SOCK_DGRAM) # UDP
sock.bind((UDP_IP, UDP_PORT))

prior = []
data = bytearray()
while True:
    data, addr = sock.recvfrom(1024) # buffer size is 1024 bytes

    # If there's data from previous frames
    if prior:
        # Concatenate it to the received data
        data = prior + data
        # Clear previous data buffer
        prior = []

    if len(data) >= 16:
        # Get the remainder
        remainder = len(data) % 16

        # Extract the valid numbers
        frame = [complex(struct.unpack('d', data[idx:idx+8])[0],
                         struct.unpack('d', data[idx+8:idx+16])[0])
                 for idx in range(0, len(data) - remainder, 16)]

        print(frame)

        if remainder:
            # Append the remainder bytes to the next block
            prior = data[-remainder::]

    else:
        prior = data


