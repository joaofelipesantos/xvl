/*
 * Copyright (C) 2019 Trinity College Dublin - CONNECT Centre.
 * This file is part of XVL <https://bitbucket.org/joaofelipesantos/xvl/>.
 *
 * XVL is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Commercial Licenses are also available from Trinity College Dublin.
 * 
 * XVL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with XVL. If not, see <http://www.gnu.org/licenses/>.
 */
#include <iostream>
#include <complex>
#include <chrono>
#include <array>
#include <thread>
#include <math.h>
#include <boost/array.hpp>
#include <boost/asio.hpp>

using boost::asio::ip::udp;

class UDPClient
{
private:
	boost::asio::io_service& io_service_;
	udp::socket socket_;
	udp::endpoint endpoint_;

public:
  // Constructor
	UDPClient(boost::asio::io_service& io_service,
            const std::string& host,
            const std::string& port
	) : io_service_(io_service), socket_(io_service, udp::endpoint(udp::v4(), 0))
  {
		udp::resolver resolver(io_service_);
		udp::resolver::query query(udp::v4(), host, port);
		udp::resolver::iterator iter = resolver.resolve(query);
		endpoint_ = *iter;
	}
  // Destructor
	~UDPClient()
	{
		socket_.close();
	}

  // Send data constantly
  template<long unsigned int SIZE> // Using a template as we don t know the
	void send(std::array<std::complex<double>, SIZE>& msg, double rate)
  {
    // Get the time between frame
    long int threshold = lrint(msg.size() / rate);

    while (true)
    {
      // Sleep and wait for the right time
      std::this_thread::sleep_for(std::chrono::microseconds(threshold));
      // Send the information
      socket_.send_to(boost::asio::buffer(msg, sizeof(msg[0]) * msg.size()), endpoint_);
    }
	}

};

int main(int argc, char* argv[])
{
  // Default variables
  std::string host = "localhost";
  std::string port = "5000";
  std::string rate = "1e-6"; // In Msps - defaults to 1 sample per second
  const unsigned int p_size = 3;

  // Shittiest way to parse CLI argument ever, but does the drill
  if (argc % 2 == 0)
  {
    std::cout << "Wrong number of arguments" << std::endl;
    return 1;
  }
  // Iterate over the arguments and change the default variables accordingly
  for (int i = 1; i < argc; i+=2)
  {
    switch (argv[i][1])
    {
    case 'h':
      host = argv[i + 1];
      break;
    case 'p':
      port = argv[i + 1];
      break;
    case 'r':
      rate = argv[i + 1];
      break;

    default: break;
    }
  }

  // Initialise the async IO service
  boost::asio::io_service io_service;
  // Initialise the UDP client
	UDPClient client(io_service, host, port);

  std::cout << "FFT Size: " << p_size << "\tSampling rate: " << rate <<
    "\tThreshold: " << p_size * 1e9 / std::stod(rate) << std::endl;
  // Construct the payload array
  std::array<std::complex<double>, p_size> payload;
  // Fill the array with IQ samples
  payload.fill(std::complex<double>(2.0, 1.0));

  // Send the payload at the given rate
	client.send(payload, std::stod(rate));
}
