#! /usr/bin/python3

# Copyright (C) 2019 Trinity College Dublin - CONNECT Centre.
# This file is part of XVL <https://bitbucket.org/joaofelipesantos/xvl/>.
#
# XVL is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# Commercial Licenses are also available from Trinity College Dublin.
# 
# XVL is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with XVL. If not, see <http://www.gnu.org/licenses/>.

import socket
from time import sleep

UDP_IP = "127.0.0.1"
UDP_PORT = 6777
MESSAGE = "Hello, World!"

print "UDP target IP:", UDP_IP
print "UDP target port:", UDP_PORT
print "message:", MESSAGE

sock = socket.socket(socket.AF_INET, # Internet
                     socket.SOCK_DGRAM) # UDP

while True:
    sleep(1/1000)
    sock.sendto(MESSAGE, (UDP_IP, UDP_PORT))
