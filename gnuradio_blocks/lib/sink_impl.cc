/* -*- c++ -*- */

/*
 * Copyright 2018 <+YOU OR YOUR COMPANY+>.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif // ifdef HAVE_CONFIG_H

#include <gnuradio/io_signature.h>
#include "sink_impl.h"

namespace gr {
  namespace xvl {

    sink::sptr
    sink::make(double             center_frequency,
               double             samp_rate,
               unsigned int       vr_id,
               const std::string &host,
               unsigned int       port,
               unsigned int       payload)
    {
      return gnuradio::get_initial_sptr
               (new sink_impl(center_frequency,
                              samp_rate,
                              vr_id,
                              host,
                              port,
                              payload));
    }

    /*
     * The private constructor
     */
    sink_impl::sink_impl(double             d_center_frequency,
                         double             d_samp_rate,
                         unsigned int       u_id,
                         const std::string &s_host,
                         unsigned int       u_port,
                         unsigned int       u_payload)
      : gr::hier_block2("sink",
                        gr::io_signature::make(1, 1, sizeof(gr_complex)),
                        gr::io_signature::make(0, 0, 0))
    {

      client = new xvl_client(s_host, u_port, u_id, true);

      client->check_connection();

      int i_tx_port = client->request_tx_resources(d_center_frequency,
                                                   d_samp_rate);

      if (i_tx_port)
      {
        d_udp_sink = gr::blocks::udp_sink::make(sizeof(gr_complex),
                                                s_host,
                                                i_tx_port,
                                                u_payload,
                                                true);

        connect(self(), 0, d_udp_sink, 0);
      }
      else
      {
        std::cerr << "Not able to reserve resources." << std::endl;
        exit(1);
      }
    }

    /*
     * Our virtual destructor.
     */
    sink_impl::~sink_impl()
    {
      // client->free_resources();
    }

    void sink_impl::start_transmission(const std::string s_host, int i_port)
    {
      d_udp_sink->connect(s_host, i_port);
    }

    void sink_impl::stop_transmission(void)
    {
      d_udp_sink->disconnect();
    }
  } /* namespace xvl */
}   /* namespace gr */
