/* -*- c++ -*- */
/*
 * Copyright 2018 <+YOU OR YOUR COMPANY+>.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_XVL_SOURCE_IMPL_H
#define INCLUDED_XVL_SOURCE_IMPL_H

#include <xvl/source.h>
#include <gnuradio/blocks/udp_source.h>
#include "xvl_client.h"

namespace gr {
  namespace xvl {
    class source_impl : public source
    {
     private:

      gr::blocks::udp_source::sptr d_udp_source;
      xvl_client* client;

     public:
      source_impl(double             d_center_frequency,
                  double             d_samp_rate,
                  unsigned int       u_id,
                  const std::string &host,
                  unsigned int       u_port,
                  unsigned int       u_payload);
      ~source_impl();

      // Where all the action really happens
      // Where all the action really happens
      void start_reception(const std::string s_host,
                           int               i_port);

      void stop_reception(void);
    };

  } // namespace xvl
} // namespace gr

#endif /* INCLUDED_XVL_SOURCE_IMPL_H */
