/* -*- c++ -*- */

#define XVL_API

%include "gnuradio.i"			// the common stuff

//load generated python docstrings
%include "xvl_swig_doc.i"

%{
#include "xvl/sink.h"
#include "xvl/source.h"
%}

%include "xvl/sink.h"
GR_SWIG_BLOCK_MAGIC2(xvl, sink);
%include "xvl/source.h"
GR_SWIG_BLOCK_MAGIC2(xvl, source);
