/* -*- c++ -*- */
/*
 * Copyright 2018 <+YOU OR YOUR COMPANY+>.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */


#ifndef INCLUDED_XVL_SOURCE_H
#define INCLUDED_XVL_SOURCE_H

#include <xvl/api.h>
#include <gnuradio/hier_block2.h>

namespace gr {
  namespace xvl {

    /*!
     * \brief <+description of block+>
     * \ingroup xvl
     *
     */
    class XVL_API source : virtual public gr::hier_block2
    {
     public:
      typedef boost::shared_ptr<source> sptr;

      /*!
       * \brief Return a shared_ptr to a new instance of xvl::source.
       *
       * To avoid accidental use of raw pointers, xvl::source's
       * constructor is in a private implementation
       * class. xvl::source::make is the public interface for
       * creating new instances.
       */
      static sptr make(double             center_frequency,
                       double             samp_rate,
                       unsigned int       vr_id,
                       const std::string &host,
                       unsigned           port,
                       unsigned           payload);
    };

  } // namespace xvl
} // namespace gr

#endif /* INCLUDED_XVL_SOURCE_H */
