/*
 * Copyright (C) 2019 Trinity College Dublin - CONNECT Centre.
 * This file is part of XVL <https://bitbucket.org/joaofelipesantos/xvl/>.
 *
 * XVL is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Commercial Licenses are also available from Trinity College Dublin.
 * 
 * XVL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with XVL. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef XVL_MAIN_INCLUDE
#define XVL_MAIN_INCLUDE

#include "xvl_core.h"
#include "xvl_server.h"
#include "xvl_stats.h"

class xvl
{
 public:

  // Pointer to the XVL Server
  std::unique_ptr<xvl_server> server;
  /// Pointer to the XVL Core
  std::shared_ptr<xvl_core> core;
  // Pointer to the XVL Statistics monitor
  std::shared_ptr<xvl_monitor> monitor;

  unsigned int u_port;

  // Constructor
  xvl(unsigned int u_control_port, unsigned int u_monitor_port = 4996);

  void set_rx_config(double d_cf, double d_bw, unsigned int u_fft_size);

  void set_tx_config(double d_cf, double d_bw, unsigned int u_fft_size);

  // Run method
  void run();
};

#endif
