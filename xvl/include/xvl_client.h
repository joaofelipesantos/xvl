/*
 * Copyright (C) 2019 Trinity College Dublin - CONNECT Centre.
 * This file is part of XVL <https://bitbucket.org/joaofelipesantos/xvl/>.
 *
 * XVL is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Commercial Licenses are also available from Trinity College Dublin.
 * 
 * XVL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with XVL. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef XVL_SERVER_INCLUDE
#define XVL_SERVER_INCLUDE

#include <zmq.hpp>
#include <string>
#include <iostream>
#include <sstream>
#include <boost/property_tree/json_parser.hpp>

class xvl_client
{
  // Server host and port
  std::string s_server_host;
  std::string s_server_port;

  // Client ID -- TODO need a better way to define it
  int u_id;
  // Debug flag
  bool b_debug_flag;

public:
    // Constructor
  xvl_client(std::string s_host = "localhost",
             const unsigned int u_port = 5000,
             const unsigned int u_client_id = 10,
             const bool b_debug = false);

  // Request RX resources
  int request_rx_resources(const double d_centre_freq = 0,
                           const double d_bandwidth = 0);

  // Request TX resources
  int request_tx_resources(const double d_centre_freq = 0,
                           const double d_bandwidth = 0);

  // Check whether the hypervisor is alive
  std::string check_connection(void);

  // Query the available resources
  std::string query_resources(void);

  // Free resources
  std::string free_resources(void);

private:
  // Base message methods
  std::string factory(const std::string &s_message);
};

#endif
