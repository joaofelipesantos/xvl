/*
 * Copyright (C) 2019 Trinity College Dublin - CONNECT Centre.
 * This file is part of XVL <https://bitbucket.org/joaofelipesantos/xvl/>.
 *
 * XVL is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Commercial Licenses are also available from Trinity College Dublin.
 * 
 * XVL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with XVL. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef XVL_SOCKET_INCLUDE
#define XVL_SOCKET_INCLUDE

#include <iostream>
#include <chrono>
#include <thread>
#include <boost/asio.hpp>
#include <boost/bind.hpp>

#include "xvl_base_types.h"

// Using name space UDP
using boost::asio::ip::udp;

class xvl_udp_socket
{
protected:
  // Default UDP buffer size
  const static unsigned int BUFFER_SIZE = 1024;
  // Async IO services
 	boost::asio::io_service io_service;
  // Socket object
  std::unique_ptr<boost::asio::ip::udp::socket> p_socket;
  // Connection endpoint
  boost::asio::ip::udp::endpoint endpoint;
  // Counter of bytes remaining from a previous transmission/reception
  unsigned int u_remainder;
  // Create remainder buffer, which just needs to be the size of an IQ sample
  std::array<char, IQ_SIZE> remainder_buffer;

  // Open socket to connect to host
  void connect(const std::string &s_host,
               const std::string &s_port,
               bool b_bind = false)
  {
    // Create an IP resolver
    boost::asio::ip::udp::resolver resolver(io_service);
    // Query routes to the host
    boost::asio::ip::udp::resolver::query query(
      s_host,
      s_port,
      boost::asio::ip::resolver_query_base::passive);

    // Resolve the address
    endpoint = *resolver.resolve(query);

    // Create the socket
    p_socket = std::make_unique<boost::asio::ip::udp::socket>(io_service);
    // Open the socket
    p_socket->open(endpoint.protocol());

    // If set to bind the socket
    if (b_bind)
    {
      p_socket->bind(endpoint);
    }

    // Reuse the address
    boost::asio::socket_base::reuse_address roption(true);
    p_socket->set_option(roption);
  };

  // Release resource
  void disconnect()
  {
    // Reset the IO service
    io_service.reset();
    // Stop the IO service
    io_service.stop();
    // Close the socket
    p_socket->close();
  };
};

class udp_source : public xvl_udp_socket, public xvl_source
{
private:
  // Received endpoint
  boost::asio::ip::udp::endpoint endpoint_rcvd;
  // UDP thread to handle the receiving of datagrams
  std::unique_ptr<std::thread> rx_udp_thread;
  // Create input buffer
  std::array<char, BUFFER_SIZE> input_buffer;
  // Reinterpreted pointer to the input buffer
  iq_sample* p_reinterpreted_cast;

public:
  // Constructor
	udp_source(const std::string& host, const std::string& port);
  // Destructor
  ~udp_source()
  {
    // Join the UDP thread that handle callbacks
    rx_udp_thread->join();

    // Disconnect socket and free resources
    disconnect();
  };

  // Registers the handle_receive method as a callback for incoming datagrams
  void receive();

private:
  // Handle datagram and buffers, outputting to a queue
  void handle_receive(const boost::system::error_code& error,
                      unsigned int u_bytes_trans);
  // Start the IO service before receiving the IQ samples
  void run_io_service() { io_service.run(); };

};

class udp_sink : public xvl_udp_socket, public xvl_sink
{
private:
  // UDP thread to handle the transmitting datagrams
  std::unique_ptr<std::thread> tx_udp_thread;
  // Thread stop condition
  bool thr_stop;

  // Pointer to output buffer
  iq_stream* p_input_buffer;

  // Create input buffer and the remainder buffers
  std::array<char, BUFFER_SIZE> output_buffer;
  // Reinterpreted pointer to the input buffer
  char* p_reinterpreted_cast;

public:
  // Constructor
	udp_sink(iq_stream* p_input_buffer,
           std::mutex* in_mtx,
           const std::string &s_host,
           const std::string &s_port);

  // Destructor
  ~udp_sink()
  {
    // Stop the thread
    thr_stop = true;
    // Join the UDP thread
    tx_udp_thread->join();

    // Disconnect socket and free resources
    disconnect();
  };

  // Assign the handle receive callback when a datagram is received
  void transmit();
};


#endif
