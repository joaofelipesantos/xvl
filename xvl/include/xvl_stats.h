/*
 * Copyright (C) 2019 Trinity College Dublin - CONNECT Centre.
 * This file is part of XVL <https://bitbucket.org/joaofelipesantos/xvl/>.
 *
 * XVL is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Commercial Licenses are also available from Trinity College Dublin.
 * 
 * XVL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with XVL. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef XVL_STATS_INCLUDE
#define XVL_STATS_INCLUDE

#include <string>
#include <deque>
#include <complex>
#include <thread>
#include <iostream>
#include <zmq.hpp>
#include <list>

// Typedefs
typedef std::complex<float> iq_sample;
typedef std::deque<iq_sample> iq_stream;

// Class that creates a monitoring service
class xvl_monitor
{
 private:
  // Server port
  std::string s_server_port;
  // Receive measurements
  void start();
  // Stats thread
  std::unique_ptr<std::thread> stats_thread;

 public:
  // Constructor
  xvl_monitor(unsigned int u_port = 4996);
  // Destructor
  ~xvl_monitor(){};

  // Run a thread to start receiving the measurements
  void run(void);
};

// Report messages back to the stats server
class xvl_report
{
 private:
  // Server port
  std::string s_server_port;
  // Identifier
  std::string s_id;
  // Pointers to the context and socket
  std::unique_ptr<zmq::context_t> context;
  std::unique_ptr<zmq::socket_t> socket;

  // Pointer to the message thread
  std::unique_ptr<std::thread> message_thread;
  // Thread stop condition
  bool thr_stop;

public:
  // Default constructor
  xvl_report(){};
  // Constructor
  xvl_report(unsigned int u_type,
             iq_stream* buffer,
             unsigned int u_port = 4996);
  // Destructor
  ~xvl_report()
  {
    // Stop the thread
    thr_stop = true;
    // Join thread
    message_thread->join();
    // Reset pointer
    message_thread.reset();

    // Reset the socket and the context pointers
    socket.reset();
    context.reset();
  };
  // Publish a given message to the server
  void push(iq_stream* buffer);
};


#endif
