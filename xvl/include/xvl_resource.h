/*
 * Copyright (C) 2019 Trinity College Dublin - CONNECT Centre.
 * This file is part of XVL <https://bitbucket.org/joaofelipesantos/xvl/>.
 *
 * XVL is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Commercial Licenses are also available from Trinity College Dublin.
 * 
 * XVL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with XVL. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef XVL_RESOURCE_INCLUDE
#define XVL_RESOURCE_INCLUDE

#include <iostream>
#include <list>
#include <math.h>
#include <mutex>
#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/ptree.hpp>

// Class that represents a spectrum chunk. Free chunks have an ID equal to zero
class chunk
{
public:
  // Centre Frequency in Hz
  double d_centre_freq;
  // Bandwidth in Hz
  double d_bandwidth;
  // Service index
  unsigned int u_id;

  // Class constructor
  chunk(const double &d_cf, const double &d_bw, const unsigned int id = 0)
  {
    d_centre_freq = d_cf;
    d_bandwidth = d_bw;
    u_id = id;
  }
  // Returns content of the class instance
  boost::property_tree::ptree output()
  {
    // Create a tree object
    boost::property_tree::ptree tree;
    // Insert the class parameters
    tree.put("id", this->u_id);
    tree.put("bandwidth", this->d_bandwidth);
    tree.put("centre_freq", this->d_centre_freq);
    // Return the tree object
    return tree;
  }
};

class rf_front_end
{
  // List of chunks
  std::list<chunk> resources;

public:
  // Class constructor
  rf_front_end(const double &d_cf,
               const double &d_bw,
               const unsigned int u_id = 0);

  // Create new chunks
  int create_chunks(const double &d_centre_freq,
                    const double &d_bandwidth,
                    const unsigned int u_id = 0);

  // List chunks
  boost::property_tree::ptree list_chunks();

  // Delete chunk from chunk list and resize/create free spaces
  void delete_chunks(const unsigned int &u_id);
};

class xvl_resource_manager
{
private:
  // List of RX chunks
  std::unique_ptr<rf_front_end> rx_resources;
  // List of TX chunks
  std::unique_ptr<rf_front_end> tx_resources;
  // Mutex to make its method thread safe
  std::mutex mtx;
  // Flags
  bool b_receiver;
  bool b_transmitter;

public:
  // Class constructor
  xvl_resource_manager();

  // Class destructor
  ~xvl_resource_manager(){};

  // Set the RX resources
  void set_rx_resources(const double &d_centre_freq, const double &d_bandwidth);
  // Set the TX resources
  void set_tx_resources(const double &d_centre_freq, const double &d_bandwidth);

  // Method to reserve RX resources to a service
  int reserve_rx_resources(const unsigned int &u_id,
                           const double &d_centre_freq,
                           const double &d_bandwidth);

  // Method to reserve TX resources to a service
  int reserve_tx_resources(const unsigned int &u_id,
                           const double &d_centre_freq,
                           const double &d_bandwidth);

  // Method to query the list of chunks
  boost::property_tree::ptree query_resources();

  // Method to free chunks of a service
  int free_resources(const unsigned int &u_id);
};

#endif
