/*
 * Copyright (C) 2019 Trinity College Dublin - CONNECT Centre.
 * This file is part of XVL <https://bitbucket.org/joaofelipesantos/xvl/>.
 *
 * XVL is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Commercial Licenses are also available from Trinity College Dublin.
 * 
 * XVL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with XVL. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef XVL_BUFFER_INCLUDE
#define XVL_BUFFER_INCLUDE

#include <thread>
#include <chrono>
#include <iostream>
#include <math.h>

#include "xvl_base_types.h"

// Buffer base class
class xvl_buffer
{
protected:
  // Hold the FFT size
  unsigned int u_fft_size;
  // Time threshold for padding/empty frames
  std::size_t threshold;
  // Pointer to the buffer thread
  std::unique_ptr<std::thread> buffer_thread;
  // Thread stop condition
  bool thr_stop;
};


class xvl_source_buffer : public xvl_buffer, public xvl_window
{
private:
  // Flag to indicate use of padding or empty frame
  bool b_pad;

public:
  // Constructor
  xvl_source_buffer(iq_stream* input_buffer, std::mutex* in_mtx,
                    double sampling_rate, unsigned int fft_size,
                    bool pad = false);

  ~xvl_source_buffer()
  {
    std::cout << "rrr" << std::endl;
    // Stop the thread
    thr_stop = true;
    // Stop the buffer thread
    buffer_thread->join();
  };

  // Method to receive data and put it into the buffer
  void run();

};

class xvl_sink_buffer : public xvl_buffer, public xvl_stream
{
public:
  // Constructor
  xvl_sink_buffer(window_stream* input_buffer,
                std::mutex* in_mtx,
                double sampling_rate,
                unsigned int fft_size);

  ~xvl_sink_buffer()
  {
    std::cout << "qqq" << std::endl;
    // Stop the thread
    thr_stop = true;
    // Stop the buffer thread
    buffer_thread->join();
  };  // Method to receive data and put it into the buffer
  void run();
};

#endif
