/*
 * Copyright (C) 2019 Trinity College Dublin - CONNECT Centre.
 * This file is part of XVL <https://bitbucket.org/joaofelipesantos/xvl/>.
 *
 * XVL is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Commercial Licenses are also available from Trinity College Dublin.
 * 
 * XVL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with XVL. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef XVL_CORE_INCLUDE
#define XVL_CORE_INCLUDE

#include <set>
#include <iostream>
#include <list>
#include <time.h>

#include "xvl_base_types.h"
#include "xvl_buffer.h"
#include "xvl_socket.h"
#include "xvl_resource.h"
#include "xvl_stats.h"

class xvl_virtual_radio
{
private:
  // Pointer to the UDP source socket
  std::unique_ptr<xvl_source> source_socket;
  // TX FFT window
  unsigned int u_tx_fft_size;
  // Points to the Source timed-buffer
  std::unique_ptr<xvl_window> source_buffer;
  // FFT TX window
  window_stream* tx_windows;

  // TODO should not exist, must come from the hypervisor
  std::mutex* hyp_mutex;

  // FFT RX window
  window_stream* rx_windows;
  // Points to the Sink timed-buffer
  std::unique_ptr<xvl_stream> sink_buffer;
  // RX FFT window
  unsigned int u_rx_fft_size;
  // Pointer to the UDP Sink socket
  std::unique_ptr<xvl_sink> sink_socket;

  // Report object
  std::unique_ptr<xvl_report> tx_report;

public:
  xvl_virtual_radio(const unsigned int &u_vr_id);

  ~xvl_virtual_radio()
  {
    // Delete source objects
    source_buffer.release();
    source_socket.release();
    // delete tx_windows;

    // Delete sink objects
    sink_buffer.release();
    sink_socket.release();
    // delete rx_windows;

    // Delete report objects
    tx_report.reset();
  };

  // Service ID
  unsigned int u_id;
  // Flag to indicate if the radio is transmitting
  bool b_defined_tx_chain;
  // Flag to indicate if the radio is receiving
  bool b_defined_rx_chain;
  // TX UDP port
  unsigned int u_tx_udp_port;
  // RX UDP port
  unsigned int u_rx_udp_port;

  int set_tx_chain(const unsigned int &u_tx_udp,
                   const double &d_tx_bw,
                   const unsigned int &u_tx_fft,
                   const bool b_pad = false);

  int set_rx_chain(const unsigned int &u_rx_udp,
                   const double &d_rx_bw,
                   const unsigned int &u_rx_fft_size);
};

class xvl_core
{
private:
  // Pointer to the resource manager object
  std::unique_ptr<xvl_resource_manager> p_resource_manager;

  // Save the transmitter info
  bool b_transmitter;
  double d_tx_bandwidth;
  unsigned int u_tx_fft_size;

  // Save the receiver info
  bool b_receiver;
  double d_rx_bandwidth;
  unsigned int u_rx_fft_size;

  // List of VRs
  std::list<std::shared_ptr<xvl_virtual_radio>> virtual_radios;

  // Set of occupied UDP ports
  std::set<unsigned int> used_ports;

  // Recursive function that calculates random ports
  unsigned int udp_port()
  {
    // Flag to check if the PRNG was initialised
    static bool seeded = false;
    // If not seeded
    if(!seeded)
    {
      // Seed the PRNG
      srand(time(NULL));
      // Toggle flag
      seeded = true;
    }

    // Create a number between 6000 and 6999
    unsigned int u_cur_port = rand() % 1000 + 6000;
    // If the port is not being used
    if (not used_ports.count(u_cur_port))
    {
      // Append it to the set
      used_ports.insert(u_cur_port);
      // And return the new value
      return u_cur_port;
    }
    // Otherwise, the port is being used
    else
    {
      // Call the function again
      return udp_port();
    }
  }

  // Get pointer to a VR based on its ID
  std::shared_ptr<xvl_virtual_radio> get_vr(const unsigned int &u_id);

public:
  // Constructor
  xvl_core();

  // Destructor
  ~xvl_core(){};

  // Set TX resources
  void set_tx_resources(const double &d_centre_freq,
                       const double &d_bandwidth,
                       const unsigned int &u_fft_size);

  // Set RX resources
  void set_rx_resources(const double &d_centre_freq,
                        const double &d_bandwidth,
                        const unsigned int &u_fft_size);

  // Request TX resources
  int request_tx_resources(const unsigned int &u_id,
                           const double &d_centre_freq,
                           const double &d_bandwidth);

  // Request RX resources
  int request_rx_resources(const unsigned int &u_id,
                           const double &d_centre_freq,
                           const double &d_bandwidth);

  // Query resources
  boost::property_tree::ptree query_resources();

  // Free resource
  int free_resources(const unsigned int &u_id);

};


#endif
