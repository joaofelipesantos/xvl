/*
 * Copyright (C) 2019 Trinity College Dublin - CONNECT Centre.
 * This file is part of XVL <https://bitbucket.org/joaofelipesantos/xvl/>.
 *
 * XVL is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Commercial Licenses are also available from Trinity College Dublin.
 * 
 * XVL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with XVL. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef XVL_BASE_INCLUDE
#define XVL_BASE_INCLUDE

#include <complex>
#include <queue>
#include <vector>
#include <mutex>

// Typdefs
typedef std::complex<float> iq_sample;
typedef std::deque<iq_sample> iq_stream;
typedef std::deque<std::vector<iq_sample>> window_stream;

// Default IQ samples size
const std::size_t IQ_SIZE = sizeof(iq_sample);


// Source base class that produces to output buffer
class xvl_source
{
 protected:
  // Create output buffer
  iq_stream output_buffer;
  // Lock access to the output buffer
  std::mutex out_mtx;

 public:
  // Returns pointer to the output buffer
  iq_stream* buffer();
  // Returns pointer to the mutex
  std::mutex* mutex();
};

// Sink base class that consumes from input buffer
class xvl_sink
{
 protected:
  // Pointer to output buffer
  iq_stream* p_input_buffer;
  // Pointer to the input buffer mutex
  std::mutex* p_in_mtx;
};


// Window base class that consumes IQ samples and outputs windows
class xvl_window
{
protected:
  // Pointer to the buffer object
  iq_stream* p_input_buffer;
  // Queue of arrays of IQ samples to be used for the FFT
  window_stream output_buffer;
  // Pointer to the mutex
  std::mutex* p_in_mtx;
  // Output mutex
  std::mutex out_mtx;
public:
  // Returns pointer to the output buffer of windows
  window_stream* windows();

  // Returns pointer to the mutex
  std::mutex* mutex();
};


// Stream base class that consumes windows samples and outputs IQ samples
class xvl_stream
{
protected:
  // Pointer to the input FFT
  window_stream* p_input_buffer;
  // Output deque
  iq_stream output_buffer;
  // Pointer to the mutex
  std::mutex* p_in_mtx;
  // Output mutex
  std::mutex out_mtx;
public:
  // Returns pointer to the output buffer, a stream of IQ samples
  iq_stream* stream();

  // Returns pointer to the mutex
  std::mutex* mutex();
};

#endif
