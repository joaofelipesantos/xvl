/*
 * Copyright (C) 2019 Trinity College Dublin - CONNECT Centre.
 * This file is part of XVL <https://bitbucket.org/joaofelipesantos/xvl/>.
 *
 * XVL is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Commercial Licenses are also available from Trinity College Dublin.
 * 
 * XVL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with XVL. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef XVL_SERVER_INCLUDE
#define XVL_SERVER_INCLUDE

#include <string>
#include <iostream>
#include <unistd.h>
#include <zmq.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/property_tree/json_parser.hpp>
#include "xvl_core.h"

struct xvl_info
{
  // Default values
  std::string s_status = "Disabled";
  std::string s_name = "XVL Hypervisor Server";
  std::string s_version = "0.1";

  // Output the struct content as a string
  boost::property_tree::ptree output()
  {
    // Create a tree object
    boost::property_tree::ptree tree;
    // Insert the class parameters
    tree.put("condition", this->s_status);
    tree.put("name", this->s_name);
    tree.put("version", this->s_version);
    // Return the tree object
    return tree;
  }
};

class xvl_server
{
  private:
    // Struct with the server info
    xvl_info server_info;
    // Server port
    std::string s_server_port;
    // Pointer to ther XVL Core
    std::shared_ptr<xvl_core> p_core;
  public:
    // Server constructor
  xvl_server(unsigned int u_port, std::shared_ptr<xvl_core> core);
    //Server destructor
  ~xvl_server(){};

  // Run the server
  int run(void);
};


/* Your function statement here */
#endif
