/*
 * Copyright (C) 2019 Trinity College Dublin - CONNECT Centre.
 * This file is part of XVL <https://bitbucket.org/joaofelipesantos/xvl/>.
 *
 * XVL is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Commercial Licenses are also available from Trinity College Dublin.
 * 
 * XVL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with XVL. If not, see <http://www.gnu.org/licenses/>.
 */
#include "xvl_buffer.h"

xvl_source_buffer::xvl_source_buffer(iq_stream* input_buffer, std::mutex* in_mtx,
                                     double sampling_rate, unsigned int fft_size,
                                     bool pad)
{
  // Time threshold - round the division to a long int
  threshold = fft_size * 1e9 / sampling_rate;
  std::cout << "TX: FFT Size: " << fft_size << "\tSampling Rate: " <<
    sampling_rate << "\tTime threshold: " << threshold << std::endl;

  // Number of IQ samples per FFT window
  u_fft_size = fft_size;
  // The input buffer
  p_input_buffer = input_buffer;
  // Get the mutex
  p_in_mtx = in_mtx;
  // Get the padding flag
  b_pad = pad;

  // Create a thread to receive the data
  buffer_thread = std::make_unique<std::thread>(&xvl_source_buffer::run, this);
}

void
xvl_source_buffer::run()
{
  // Thread stop condition
  thr_stop = false;
  // Vector of IQ samples that comprise a FFT window
  std::vector<iq_sample> window;
  // Reserve number of elements
  window.reserve(u_fft_size);
  // Empty IQ sample, as zeroes
  iq_sample empty_iq = {0.0, 0.0};
  // Integer to hold the current size of the queue
  long long int ll_cur_size;

  // Consume indefinitely
  while(true)
  {
    // Wait for "threshold" nanoseconds
    std::this_thread::sleep_for(std::chrono::nanoseconds(threshold));
    // If the destructor has been called
    if (thr_stop){return;}
    // Lock access to the buffer

    // Windows not being consumed
    if (output_buffer.size() > 1e3)
      {
        std::cerr << "Too many windows!" << std::endl;
      }
    // Plenty of space
    else
    {
      p_in_mtx->lock();
      // Get the current size of the queue
      ll_cur_size = p_input_buffer->size();
      // Check whether the buffer has enough IQ samples
      if (ll_cur_size >= u_fft_size)
      {
        // Insert IQ samples from the input buffer into the window
        window.assign(p_input_buffer->begin(),
                      p_input_buffer->begin() + u_fft_size); // 0..C

        // Erase the beginning of the queue
        p_input_buffer->erase(p_input_buffer->begin(),
                              p_input_buffer->begin() + u_fft_size);

        // Release access to the buffer
        p_in_mtx->unlock();

        // Lock access to the output buffer
        out_mtx.lock();
        // Add the window to the output buffer
        output_buffer.push_back(window);
        // Unlock access to the output buffer
        out_mtx.unlock();
      }
      // Otherwise, the buffer is not ready yet
      else if (b_pad)
      {
        // Copy the current amount of samples from the buffer to the window
        window.assign(p_input_buffer->begin(),
                      p_input_buffer->begin() + ll_cur_size); // 0..C-1
        // Erase the beginning of the queue
        p_input_buffer->erase(p_input_buffer->begin(),
                              p_input_buffer->begin() + ll_cur_size);

        // Release access to the buffer
        p_in_mtx->unlock();

        // Fill the remainder of the window with complex zeroes
        window.insert(window.begin() + ll_cur_size,
                      u_fft_size - ll_cur_size,
                      empty_iq); // C..F-1

        // Lock access to the output buffer
        out_mtx.lock();
        // Add the window to the output buffer
        output_buffer.push_back(window);
        // Unlock access to the output buffer
        out_mtx.unlock();
      }
      // Without padding, just transmit an empty window and wait for the next one
      else
      {
        // Release access to the input buffer
        p_in_mtx->unlock();

        // Fill the window with complex zeroes
        window.assign(u_fft_size, empty_iq);

        // Lock access to the output buffer
        out_mtx.lock();
        // Add the window to the output buffer
        output_buffer.push_back(window);
        // Unlock access to the output buffer
        out_mtx.unlock();
      }
    } // Too much data check
    // std::cout << output_buffer.size() << std::endl;
  } // End of loop
} // End of method

xvl_sink_buffer::xvl_sink_buffer(window_stream* input_buffer, std::mutex* in_mtx,
                                 double sampling_rate, unsigned int fft_size)
{
  // Time threshold - round the division to a long int
  threshold = fft_size * 1e9 / sampling_rate;
  std::cout << "RX: FFT Size: " << fft_size << "\tSampling Rate: " <<
    sampling_rate << "\tTime threshold: " << threshold << std::endl;

  // Number of IQ samples per FFT window
  u_fft_size = fft_size;
  // The input buffer
  p_input_buffer = input_buffer;
  // Get the input mutex
  p_in_mtx = in_mtx;

  // Create a thread to receive the data
  buffer_thread = std::make_unique<std::thread>(&xvl_sink_buffer::run, this);
}

void
xvl_sink_buffer::run()
{
  // Thread stop condition
  thr_stop = false;
  // Empty IQ sample, as zeroes
  iq_sample empty_iq = {0.0, 0.0};
  // Temporary array to hold a window worth of IQ samples
  std::vector<iq_sample> window;
  window.reserve(u_fft_size);

  // Produce indefinitely
  while(true)
  {
    // Wait for "threshold" nanoseconds
    std::this_thread::sleep_for(std::chrono::nanoseconds(threshold));
    // If the destructor has been called
    if (thr_stop){return;}

    // Data not being consumed
    if (output_buffer.size() > 1e8)
    {
      std::cerr << "Too much data!" << std::endl;
    }
    // Plenty of space
    else
    {
      // Lock access to the buffer
      p_in_mtx->lock();

      // Check if there's a valid window ready for transmission
      if (not p_input_buffer->empty())
      {
        // Copy the first window from the input buffer
        window = p_input_buffer->front();
        // Pop the oldest window
        p_input_buffer->pop_front();

        // Release access to the input buffer
        p_in_mtx->unlock();


        // Lock access to the output buffer
        out_mtx.lock();
        // Insert IQ samples of the oldest window in the output deque
        output_buffer.insert(output_buffer.end(),
                                window.begin(),
                                window.end());
        // Release access to the output buffer
        out_mtx.unlock();
      }
      // If the queue of windows is empty at the moment
      else
      {
        // Release access to the buffer
        p_in_mtx->unlock();

        // Lock access to the output buffer
        out_mtx.lock();
        // Stream empty IQ samples that comprise the window duration
        output_buffer.insert(output_buffer.end(), u_fft_size, empty_iq);
        // Release access to the output buffer
        out_mtx.unlock();
      } // End padding
    } // End overflow
  } // End data check
}
