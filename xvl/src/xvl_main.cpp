/*
 * Copyright (C) 2019 Trinity College Dublin - CONNECT Centre.
 * This file is part of XVL <https://bitbucket.org/joaofelipesantos/xvl/>.
 *
 * XVL is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Commercial Licenses are also available from Trinity College Dublin.
 * 
 * XVL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with XVL. If not, see <http://www.gnu.org/licenses/>.
 */
#include "xvl_main.h"


// Real radio centre frequency, bandwidth; control port; hypervisor's sampling rate, FFT size
xvl::xvl(unsigned int u_control_port, unsigned int u_monitor_port)
{
  // Set the control port
  u_port = u_control_port;

  // Initialise the stats reporter
  monitor = std::make_shared<xvl_monitor>(u_monitor_port);
  // Initialise the core XVL
  core = std::make_shared<xvl_core>();
}

void xvl::set_rx_config(double d_cf, double d_bw, unsigned int u_fft_size)
{
  // Configure receiver resources
  core->set_rx_resources(d_cf, d_bw, u_fft_size);
}

void xvl::set_tx_config(double d_cf, double d_bw, unsigned int u_fft_size)
{
  // Configure transmitter resources
  core->set_tx_resources(d_cf, d_bw, u_fft_size);
}

  // Run server
void xvl::run()
{
  // Initialise the server
  server = std::make_unique<xvl_server>(u_port, core);

  // Run the statistics reporting server
  monitor->run();

  // Run the XVL server
  server->run();
}
