/*
 * Copyright (C) 2019 Trinity College Dublin - CONNECT Centre.
 * This file is part of XVL <https://bitbucket.org/joaofelipesantos/xvl/>.
 *
 * XVL is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Commercial Licenses are also available from Trinity College Dublin.
 * 
 * XVL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with XVL. If not, see <http://www.gnu.org/licenses/>.
 */
#include "xvl_base_types.h"

iq_stream*
xvl_source::buffer()
{
  return &output_buffer;
}

std::mutex*
xvl_source::mutex()
{
  return &out_mtx;
}

window_stream*
xvl_window::windows()
{
  return &output_buffer;
}

std::mutex*
xvl_window::mutex()
{
  return &out_mtx;
}

iq_stream*
xvl_stream::stream()
{
  return &output_buffer;
}

std::mutex*
xvl_stream::mutex()
{
  return &out_mtx;
}
