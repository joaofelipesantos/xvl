/*
 * Copyright (C) 2019 Trinity College Dublin - CONNECT Centre.
 * This file is part of XVL <https://bitbucket.org/joaofelipesantos/xvl/>.
 *
 * XVL is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Commercial Licenses are also available from Trinity College Dublin.
 * 
 * XVL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with XVL. If not, see <http://www.gnu.org/licenses/>.
 */
#include "xvl_core.h"

xvl_virtual_radio::xvl_virtual_radio(const unsigned int &u_vr_id)
{
  // Set the radio identifier
  u_id = u_vr_id;

  // Set flags to false
  b_defined_tx_chain = false;
  b_defined_rx_chain = false;
}

int
xvl_virtual_radio::set_tx_chain(const unsigned int &u_tx_udp,
                                const double &d_tx_bw,
                                const unsigned int &u_tx_fft,
                                const bool b_pad)
{
  // If already transmitting
  if (b_defined_tx_chain)
  {
    // Return error
    return 1;
  }
  // Set the VR TX UDP port
  u_tx_udp_port = u_tx_udp;
  // Set the VR TX FFT size
  u_tx_fft_size = u_tx_fft;
  // Create UDP source
  source_socket = std::make_unique<udp_source>("0.0.0.0",
                                               std::to_string(u_tx_udp_port));
  // Create new timed buffer
  source_buffer = std::make_unique<xvl_source_buffer>(source_socket->buffer(),
                                                      source_socket->mutex(),
                                                      d_tx_bw,
                                                      u_tx_fft_size,
                                                      b_pad);
  // Get the pointer to the FFT window array
  tx_windows = source_buffer->windows();
  // Toggle transmitting flag
  b_defined_tx_chain = true;

  // Create reports object
  tx_report = std::make_unique<xvl_report>(u_id, source_socket->buffer());

  // Successful return
  return 0;
}

int
xvl_virtual_radio::set_rx_chain(const unsigned int &u_rx_udp,
                                const double &d_rx_bw,
                                const unsigned int &u_rx_fft)
{
  // If already receiving
  if (b_defined_rx_chain)
    {
      // Return error
      return 1;
    }
  // Set the VR RX UDP port
  u_rx_udp_port = u_rx_udp;
  // Set the VR RX FFT size
  u_rx_fft_size = u_rx_fft;

  // TODO this must be shared with the hypervisor, or come from it
  hyp_mutex = new std::mutex;
  rx_windows = new window_stream;
  // Create new TX timed buffer
  sink_buffer = std::make_unique<xvl_sink_buffer>(rx_windows,
                                                hyp_mutex,
                                                d_rx_bw,
                                                u_rx_fft_size);

  // Create UDP transmitter
  sink_socket = std::make_unique<udp_sink>(sink_buffer->stream(),
                                           sink_buffer->mutex(),
                                           "0.0.0.0",
                                           std::to_string(u_rx_udp_port));

  // Toggle transmitting flag
  b_defined_rx_chain = true;

  // TODO Create RX reports object
  // rx_report = std::make_unique<xvl_report>(u_id, sink_socket->windows());

  // Successful return
  return 0;
}


// Real radio centre frequency, bandwidth; and the hypervisor's sampling rate, FFT size
xvl_core::xvl_core()
{
  // Initialise the resource manager
  p_resource_manager = std::make_unique<xvl_resource_manager>();

  // Set flags to false and wait for initialisation
  b_receiver = false;
  b_transmitter = false;

  //TODO Initialise the hypervisor
}

void
xvl_core::set_tx_resources(const double &d_centre_freq,
                           const double &d_bandwidth,
                           const unsigned int &u_fft_size)
{
  // If already configured the transmitter
  if (b_transmitter)
    {
      std::cerr << "Already configured the transmitter!"  << std::endl;
      exit(40);
    }

  // Save the hypervisor's bandwidth and FFT size
  d_tx_bandwidth = d_bandwidth ;
  u_tx_fft_size = u_fft_size;

  // Initialise the RX resources
  p_resource_manager->set_tx_resources(d_centre_freq, d_bandwidth);

  // Toggle flag
  b_transmitter = true;
}

void
xvl_core::set_rx_resources(const double &d_centre_freq,
                           const double &d_bandwidth,
                           const unsigned int &u_fft_size)
{
  // If already configured the receiver
  if (b_receiver)
  {
    std::cerr << "Already configured the receiver!"  << std::endl;
    exit(40);
  }

  // Save the hypervisor's bandwidth and FFT size
  d_rx_bandwidth = d_bandwidth ;
  u_rx_fft_size = u_fft_size;

  // Initialise the RX resources
  p_resource_manager->set_rx_resources(d_centre_freq, d_bandwidth);

  // Toggle flag
  b_receiver = true;
}

int
xvl_core::request_tx_resources(const unsigned int &u_id,
                               const double &d_centre_freq,
                               const double &d_bandwidth)
{
  // If not configured to transmit
  if (not b_transmitter)
    {
      // Return error -- zero is bad
      return 0;
  }

  // Try to find the given VR
  auto vr = get_vr(u_id);
  // If it does not exist
  if (vr == nullptr)
    {
      // Create a new virtual radio
      virtual_radios.emplace_back(std::make_unique<xvl_virtual_radio>(u_id));

      // Point the iterator to the new last element
      vr = virtual_radios.back();
    }
  // If the VR is already transmitting
  else if (vr->b_defined_tx_chain)
    {
      // Return error -- zero is bad
      return 0;
    }

  // Try to reserve the resource chunks
  if(p_resource_manager->reserve_tx_resources(u_id, d_centre_freq, d_bandwidth))
    {
      // Return error -- zero is bad
      return 0;
  }

  // Create TX UDP port
  unsigned int u_udp_port = udp_port();

  // Calculate the virtual radio TX FFT size
  unsigned int u_vr_fft_size = u_tx_fft_size * (d_bandwidth / d_tx_bandwidth);

  vr->set_tx_chain(u_udp_port, d_bandwidth, u_vr_fft_size);

   // If able to create all of it, return the port number
  return u_udp_port;
}

int
xvl_core::request_rx_resources(const unsigned int &u_id,
                               const double &d_centre_freq,
                               const double &d_bandwidth)
{
  // If not configured to receive
  if (not b_receiver)
  {
    // Return error -- zero is bad
    return 0;
  }

  // Try to find the given VR
  auto vr = get_vr(u_id);
  // If it does not exist
  if (vr == nullptr)
  {
    // Create a new virtual radio
    virtual_radios.emplace_back(std::make_unique<xvl_virtual_radio>(u_id));

    // Point the iterator to the new last element
    vr = virtual_radios.back();
  }
  // If the VR is already receiving
  else if (vr->b_defined_rx_chain)
  {
    // Return error -- zero is bad
    return 0;
  }

  // Try to reserve the resource chunks
  if(p_resource_manager->reserve_rx_resources(u_id, d_centre_freq, d_bandwidth))
  {
    // Return error -- zero is bad
    return 0;
  }

  // Create RX UDP port
  unsigned int u_udp_port = udp_port();

  // Calculate the virtual radio RX FFT size
  unsigned int u_vr_fft_size = u_rx_fft_size * (d_bandwidth / d_rx_bandwidth);

  vr->set_rx_chain(u_udp_port, d_bandwidth, u_vr_fft_size);

   // If able to create all of it, return the port number
  return u_udp_port;
}

// Query the virtual radios (and add their UDP port)
boost::property_tree::ptree
xvl_core::query_resources(void)
{
  // Get the query from the RM
  boost::property_tree::ptree query_message = \
    p_resource_manager->query_resources();

  if (b_receiver)
  {
    // Get the RX child
    auto rx_chunks = query_message.get_child("receiver");
    // TODO There must be an easier way to do this
    query_message.erase("receiver");

    // Iterate over the child
    for (auto p_chunk = rx_chunks.begin();
         p_chunk != rx_chunks.end(); p_chunk++)
    {
      // If the current entry is a valid type
      if (p_chunk->second.get<int>("id"))
      {
        std::shared_ptr<xvl_virtual_radio> vr = \
          get_vr(p_chunk->second.get<int>("id"));

        // If receiving
        if ((vr != nullptr) and vr->b_defined_rx_chain)
        {
          // Add this new entry
          p_chunk->second.put("rx_udp_port",vr->u_rx_udp_port);
        }
      }
    }
    // Update the original tree
    query_message.add_child("receiver", rx_chunks);
  }

  if (b_transmitter)
  {
    // Get the TX child
    auto tx_chunks = query_message.get_child("transmitter");
    // TODO There must be an easier way to do this
    query_message.erase("transmitter");

    // Iterate over the child
    for (auto p_chunk = tx_chunks.begin();
         p_chunk != tx_chunks.end(); p_chunk++)
    {
      // If the current entry is a valid type
      if (p_chunk->second.get<int>("id"))
      {
        auto vr = get_vr(p_chunk->second.get<int>("id"));

        // If receiving
        if ((vr != nullptr) and vr->b_defined_tx_chain)
        {
          // Add this new entry
          p_chunk->second.put("tx_udp_port",vr->u_tx_udp_port);
        }
      }
    }
    // Update the original tree
    query_message.add_child("transmitter", tx_chunks);
  }

  // Return updated
  return query_message;
}

// Deletes a given virtual radio
int
xvl_core::free_resources(const unsigned int &u_id)
{
  // Try to free the resources
  bool b_freed = p_resource_manager->free_resources(u_id);

  // If not able to free
  if (b_freed)
  {
    // Return error -- zero is bad
    return 0;
  }
  else
  {
    // Iterate over the VR list
    for (auto vr = virtual_radios.begin(); vr != virtual_radios.end(); vr++)
    {
      // If the VR identifier matches the entered one
      if ((*vr)->u_id == u_id)
      {
        // Remove this given VR
        vr = virtual_radios.erase(vr);
      }
    } // VR loop
    return 1;
  }
}

std::shared_ptr<xvl_virtual_radio>
xvl_core::get_vr(const unsigned int &u_id)
{
  // Iterate over the VRs
  for (auto vr = virtual_radios.begin(); vr != virtual_radios.end(); vr++)
  {
    // If foind the right VR
    if ((*vr)->u_id == u_id)
    {
      // Return a point to it
      return (*vr);
    }
  }
  // Otherwise, return a null pointer
  return nullptr;
}
